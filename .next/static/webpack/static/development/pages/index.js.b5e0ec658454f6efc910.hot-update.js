webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/contacts/contacts.js":
/*!*****************************************!*\
  !*** ./components/contacts/contacts.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_no_ssr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-no-ssr */ "./node_modules/react-no-ssr/index.js");
/* harmony import */ var react_no_ssr__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_no_ssr__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
var _jsxFileName = "d:\\cv-khachik\\cv-khachik\\components\\contacts\\contacts.js";



var contactUsObjKeys = [{
  iconPrefix: 'fas',
  icon: 'envelope',
  name: 'Gmail'
}, {
  iconPrefix: 'fab',
  icon: 'skype',
  name: 'Skype'
}, {
  iconPrefix: 'fas',
  icon: 'mobile',
  name: 'Phone'
}, {
  iconPrefix: 'fas',
  icon: 'map-marker-alt',
  name: 'Address'
}];
var contactUsObjValues = ['karaqeshishyan.99@gmail.com', 'Khachik Karakeshishyan', '+37491651165', 'Yerevan, Armenia'];

function Contacts() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "section contacts",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: "my-3 text-center text-white",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, "Contacts"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "contacts-description",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, "if you have questions or suggestions you can contact me with the mentioned contacts."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row justify-content-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "col-12 col-md-8 col-lg-6 my-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    className: "px-5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "contacts-wrapper-input",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    name: "name",
    placeholder: "Name",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "contacts-wrapper-input",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "email",
    name: "email",
    placeholder: "E-mail",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "contacts-wrapper-input",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("textarea", {
    type: "text",
    name: "message",
    placeholder: "Message",
    rows: "4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "contacts-wrapper-input pt-2 text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "button",
    type: "submit",
    name: "submit",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, "Send messeage")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "contacts-me col-12 col-md-8 col-lg-6 my-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row justify-content-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "contacts-me-bracket-left col-2 px-0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "static/icons/bracket-left.png",
    title: "bracket",
    alt: "icon",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "contacts-me-obj-keys col-7 col-sm-8 px-0 py-4 d-flex justify-content-around",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "contacts-me-obj-keys d-flex flex-column justify-content-between",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  }, contactUsObjKeys.map(function (key, index) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      key: index,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_no_ssr__WEBPACK_IMPORTED_MODULE_1___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    }, key.iconPrefix == 'fab' ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
      icon: ['fab', key.icon],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 57
      },
      __self: this
    }) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], {
      icon: key.icon,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    })), "\xA0", key.name, "\xA0", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      className: "contacts-me-obj-keys-dots",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63
      },
      __self: this
    }, ":"));
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "contacts-me-obj-values d-flex flex-column justify-content-between",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67
    },
    __self: this
  }, contactUsObjValues.map(function (value, index) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      key: index,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, value);
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "contacts-me-bracket-right col-2 px-0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "static/icons/bracket-right.png",
    title: "bracket",
    alt: "icon",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72
    },
    __self: this
  })))))));
}

/* harmony default export */ __webpack_exports__["default"] = (Contacts);

/***/ })

})
//# sourceMappingURL=index.js.b5e0ec658454f6efc910.hot-update.js.map