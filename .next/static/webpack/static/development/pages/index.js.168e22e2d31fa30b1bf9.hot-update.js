webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/header/header.js":
/*!*************************************!*\
  !*** ./components/header/header.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/slicedToArray */ "./node_modules/@babel/runtime-corejs2/helpers/esm/slicedToArray.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "d:\\cv-khachik\\cv-khachik\\components\\header\\header.js";


var navbar = [{
  href: '#home',
  name: 'Home'
}, {
  href: '#skills',
  name: 'Skills'
}, {
  href: '#aboutMe',
  name: 'About Me'
}, {
  href: '#contacts',
  name: 'Contacts'
}];

function Header(_ref) {
  var setFrameLinks = _ref.setFrameLinks,
      handleClickLinks = _ref.handleClickLinks,
      headerRef = _ref.headerRef,
      frameHeight = _ref.frameHeight,
      frameWidth = _ref.frameWidth,
      frameMargin = _ref.frameMargin;
  var buttonRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])({}),
      _useState2 = Object(_babel_runtime_corejs2_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useState, 2),
      frameSize = _useState2[0],
      setFrameSize = _useState2[1];

  var handleClick = function handleClick(link, index) {
    handleClickLinks(link, index);
  };

  var windowClick = function windowClick(e) {
    if (buttonRef.current.attributes[5].value == 'true') {
      // Check when menu is open then we are moving forward
      var path = e.path || e.composedPath(); // Get path of event

      if (!path.includes(headerRef.current) && window.innerWidth < 992) {
        // Check when didn't clicked at header and window display is small and if it is true new closes the menu of header
        buttonRef.current.click();
      }
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (window.innerWidth < 992) {
      // Large devices
      setFrameSize({
        width: frameWidth + 'px',
        height: frameHeight + 'px',
        marginTop: frameMargin + 'px'
      });
    } else {
      // Small devices
      setFrameSize({
        width: frameWidth + 'px',
        marginLeft: frameMargin + 'px'
      });
    }
  }, [frameWidth, frameMargin]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    window.addEventListener('click', windowClick);
    return function () {
      window.removeEventListener('click', windowClick);
    };
  }, []);
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("header", {
    ref: headerRef,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("nav", {
    className: "navbar navbar-expand-lg navbar-light",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
    href: "/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h3", {
    className: "navbar-brand logo m-0",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63
    },
    __self: this
  }, "CV-Khachik")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    ref: buttonRef,
    onClick: function onClick() {
      return setFrameLinks();
    },
    className: "navbar-toggler",
    type: "button",
    "data-toggle": "collapse",
    "data-target": "#navbarNav",
    "aria-controls": "navbarNav",
    "aria-expanded": "false",
    "aria-label": "Toggle navigation",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    className: "navbar-toggler-icon",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "collapse navbar-collapse",
    id: "navbarNav",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
    className: "navbar-nav ml-auto position-relative",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "frame-link",
    style: frameSize,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83
    },
    __self: this
  }), navbar.map(function (item, index) {
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
      className: "nav-item",
      key: index,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
      href: item.href,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
      className: "nav-link",
      onClick: function onClick() {
        return handleClick(item, index);
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 87
      },
      __self: this
    }, item.name)));
  })))));
}

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ })

})
//# sourceMappingURL=index.js.168e22e2d31fa30b1bf9.hot-update.js.map