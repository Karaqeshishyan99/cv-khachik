webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/skills/skills.js":
/*!*************************************!*\
  !*** ./components/skills/skills.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/slicedToArray */ "./node_modules/@babel/runtime-corejs2/helpers/esm/slicedToArray.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "d:\\cv-khachik\\cv-khachik\\components\\skills\\skills.js";


var _ref = typeof window !== 'undefined' && __webpack_require__(/*! wowjs */ "./node_modules/wowjs/dist/wow.js"),
    WOW = _ref.WOW;

var makeing_layout = [{
  img: 'html5.png',
  title: 'HTML5',
  alt: 'skill logos',
  animPos: 'bounceInLeft',
  animDelay: '0.5s'
}, {
  img: 'css3.png',
  title: 'CSS3',
  alt: 'skill logos',
  animPos: 'bounceInLeft',
  animDelay: '0.2s'
}, {
  img: 'sass.png',
  title: 'SASS',
  alt: 'skill logos',
  animPos: 'bounceInRight',
  animDelay: '0.2s'
}, {
  img: 'bootstrap.png',
  title: 'Bootstrap4',
  alt: 'skill logos',
  animPos: 'bounceInRight',
  animDelay: '0.5s'
}];
var js_frontend = [{
  img: 'js.png',
  title: 'JavaScript',
  alt: 'skill logos',
  animPos: 'bounceInLeft',
  animDelay: '0.3s'
}, {
  img: 'jquery.png',
  title: 'Jquery',
  alt: 'skill logos',
  animPos: 'bounceInRight',
  animDelay: '0.3s'
}];
var js_frameworks = [{
  img: 'reactjs.png',
  title: 'ReactJS',
  alt: 'skill logos',
  animPos: 'bounceInLeft',
  animDelay: '0.3s'
}, {
  img: 'redux.png',
  title: 'Reudx',
  alt: 'skill logos',
  animPos: 'bounceInUp',
  animDelay: '0.3s'
}, {
  img: 'nextjs.png',
  title: 'NextJS',
  alt: 'skill logos',
  animPos: 'bounceInRight',
  animDelay: '0.3s'
}];
var php = [{
  img: 'php.png',
  title: 'PHP',
  alt: 'skill logos',
  animPos: 'bounceInLeft',
  animDelay: '0.3s'
}, {
  img: 'sql.png',
  title: 'SQL',
  alt: 'skill logos',
  animPos: 'bounceInRight',
  animDelay: '0.3s'
}];
var programing_skills = [{
  img: 'mvc.png',
  title: 'MVC',
  alt: 'skill logos',
  animPos: 'bounceInLeft',
  animDelay: '0.3s'
}, {
  img: 'oop.png',
  title: 'OOP',
  alt: 'skill logos',
  animPos: 'bounceInUp',
  animDelay: '0.3s'
}, {
  img: 'seo.jpg',
  title: 'SEO',
  alt: 'skill logos',
  animPos: 'bounceInRight',
  animDelay: '0.3s'
}];

function Skills(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState2 = Object(_babel_runtime_corejs2_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useState, 2),
      wowAnimation = _useState2[0],
      setWowAnimation = _useState2[1];

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    // if (wowAnimation != 'done') {
    var wow = new WOW({
      live: false
    });
    wow.init(); // 	setWowAnimation('done');
    // }
  }, []);
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "section skills",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "my-3 text-center",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", {
    className: "skills-title",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, "Skills"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    className: "skills-description",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }, "My professional skills in web development")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "skills-part makeing-layout",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: this
  }, makeing_layout.map(function (item, index) {
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "skills-part-item wow ".concat(item.animPos),
      "data-wow-duration": "2s",
      "data-wow-delay": item.animDelay,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      src: "static/skills_logos/".concat(item.img),
      title: item.title,
      alt: item.alt,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59
      },
      __self: this
    }));
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "skills-part js-frontend",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63
    },
    __self: this
  }, js_frontend.map(function (item, index) {
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "skills-part-item wow ".concat(item.animPos),
      "data-wow-duration": "2s",
      "data-wow-delay": item.animDelay,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      src: "static/skills_logos/".concat(item.img),
      title: item.title,
      alt: item.alt,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71
      },
      __self: this
    }));
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "skills-part js-frameworks",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: this
  }, js_frameworks.map(function (item, index) {
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "skills-part-item wow ".concat(item.animPos),
      "data-wow-duration": "2s",
      "data-wow-delay": item.animDelay,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      src: "static/skills_logos/".concat(item.img),
      title: item.title,
      alt: item.alt,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83
      },
      __self: this
    }));
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "skills-part php",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87
    },
    __self: this
  }, php.map(function (item, index) {
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "skills-part-item wow ".concat(item.animPos),
      "data-wow-duration": "2s",
      "data-wow-delay": item.animDelay,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 89
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      src: "static/skills_logos/".concat(item.img),
      title: item.title,
      alt: item.alt,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95
      },
      __self: this
    }));
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "skills-part programing-skills",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 99
    },
    __self: this
  }, programing_skills.map(function (item, index) {
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "skills-part-item wow ".concat(item.animPos),
      "data-wow-duration": "2s",
      "data-wow-delay": item.animDelay,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 101
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      src: "static/skills_logos/".concat(item.img),
      title: item.title,
      alt: item.alt,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 107
      },
      __self: this
    }));
  }))));
}

/* harmony default export */ __webpack_exports__["default"] = (Skills);

/***/ })

})
//# sourceMappingURL=index.js.b0b4b46534fe8fc7cf01.hot-update.js.map