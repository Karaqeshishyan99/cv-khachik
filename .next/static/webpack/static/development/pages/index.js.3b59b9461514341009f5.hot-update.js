webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/home/home.js":
/*!*********************************!*\
  !*** ./components/home/home.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _experience_experience__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../experience/experience */ "./components/experience/experience.js");
var _jsxFileName = "d:\\cv-khachik\\cv-khachik\\components\\home\\home.js";



function Home(_ref) {
  var pictureHeight = _ref.pictureHeight;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
    className: "home",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "for-home-info w-100 d-flex justify-content-center",
    style: {
      height: pictureHeight.current ? pictureHeight.current.offsetHeight ? pictureHeight.current.offsetHeight : '100vh' : '100vh'
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "home-info",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "home-info-title",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }, "Welcome to my CV site"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "home-info-description",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }, "I'm a web developer. This is my CV site where you can get acquainted with my works and my professional skills and also you can contact me\u2024")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_experience_experience__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }));
}

/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ })

})
//# sourceMappingURL=index.js.3b59b9461514341009f5.hot-update.js.map