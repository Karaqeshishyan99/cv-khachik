webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/experience/experience.js":
/*!*********************************************!*\
  !*** ./components/experience/experience.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/extends */ "./node_modules/@babel/runtime-corejs2/helpers/esm/extends.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js");
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "./node_modules/@fortawesome/react-fontawesome/index.es.js");
/* harmony import */ var react_no_ssr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-no-ssr */ "./node_modules/react-no-ssr/index.js");
/* harmony import */ var react_no_ssr__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_no_ssr__WEBPACK_IMPORTED_MODULE_4__);

var _jsxFileName = "d:\\cv-khachik\\cv-khachik\\components\\experience\\experience.js";




var slider_items = [{
  img: 'annakaraloans.JPG',
  companyName: 'Anna Kara Loans',
  webSite: 'https://www.annakaraloans.com/mortgage-calc-new',
  description: "I have been making mortgage calculator with javascript for annakaraloans's website.",
  location: 'Glendale, United State',
  date: 'Within 2 months'
}, {
  img: 'trainingDevelopment.jpg',
  description: 'I have been doing online training with Frontend development and teached HTML, CSS, Bootstrap, JavaScript, jQuery skills in two months',
  location: 'Glendale, United State',
  date: 'Within 2 months'
}, {
  img: 'projectnulla.svg',
  companyName: 'Project Nulla',
  webSite: 'https://projectnulla.com',
  description: 'In the Project nulla I worked as frontend developer. There I have been doing React and Redux',
  location: 'Yerevan, Armenia',
  date: '2018 October - 2019 March'
}, {
  img: 'vectus.JPG',
  companyName: 'Vectus llc',
  webSite: 'https://vectus.am',
  description: 'In the Vectus I worked as full stack web developer. There I have been doing the Drupal framework of php',
  location: 'Yerevan, Armenia',
  date: '2019 March - July'
}];

function Experience() {
  var settings = {
    dots: false,
    className: 'center',
    infinite: true,
    centerPadding: '60px',
    slidesToShow: 3,
    swipeToSlide: true,
    responsive: [{
      breakpoint: 992,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 576,
      settings: {
        slidesToShow: 1
      }
    }]
  };
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
    className: "section experience-slider py-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_no_ssr__WEBPACK_IMPORTED_MODULE_4___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_slick__WEBPACK_IMPORTED_MODULE_2___default.a, Object(_babel_runtime_corejs2_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({}, settings, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68
    },
    __self: this
  }), slider_items.map(function (item, index) {
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      key: index,
      className: "experience-slider-item",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, item.img ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "experience-slider-item-image mb-2",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
      src: "static/company_logos/".concat(item.img),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73
      },
      __self: this
    })) : null, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "experience-slider-item-info px-3",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76
      },
      __self: this
    }, item.location ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "experience-slider-item-info-location",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_no_ssr__WEBPACK_IMPORTED_MODULE_4___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__["FontAwesomeIcon"], {
      icon: "map-marker-alt",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82
      },
      __self: this
    }, item.location)) : null, item.date ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "experience-slider-item-info-date mb-4",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_no_ssr__WEBPACK_IMPORTED_MODULE_4___default.a, {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 87
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_3__["FontAwesomeIcon"], {
      icon: "calendar-alt",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88
      },
      __self: this
    })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90
      },
      __self: this
    }, item.date)) : null, item.companyName ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "experience-slider-item-info-company mb-1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 94
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", {
      className: "experience-slider-item-info-company-title mb-0",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95
      },
      __self: this
    }, "Company Name:\xA0"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
      className: "experience-slider-item-info-company-name",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 98
      },
      __self: this
    }, item.companyName)) : null, item.webSite ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "experience-slider-item-info-company mb-1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", {
      className: "experience-slider-item-info-company-title mb-0",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105
      },
      __self: this
    }, "Web Site:", ' '), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
      className: "experience-slider-item-info-company-link",
      href: item.webSite,
      target: "_blank",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 108
      },
      __self: this
    }, item.webSite)) : null, item.description ? react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      className: "mb-1",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h5", {
      className: "experience-slider-item-info-company-title mb-0 d-inline",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 119
      },
      __self: this
    }, "Description:\xA0"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
      className: "d-inline experience-slider-item-info-company-value",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 122
      },
      __self: this
    }, item.description)) : null));
  })))));
}

/* harmony default export */ __webpack_exports__["default"] = (react__WEBPACK_IMPORTED_MODULE_1___default.a.memo(Experience));

/***/ })

})
//# sourceMappingURL=index.js.dd92ce63685947fa0970.hot-update.js.map