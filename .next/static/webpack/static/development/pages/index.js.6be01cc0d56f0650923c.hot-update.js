webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/slicedToArray */ "./node_modules/@babel/runtime-corejs2/helpers/esm/slicedToArray.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _pageHead__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pageHead */ "./pages/pageHead.js");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../style.scss */ "./style.scss");
/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.css */ "./node_modules/bootstrap/dist/css/bootstrap.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.min.css */ "./node_modules/bootstrap/dist/css/bootstrap.min.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _components_header_header__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/header/header */ "./components/header/header.js");
/* harmony import */ var _components_home_home__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/home/home */ "./components/home/home.js");
/* harmony import */ var _components_fixedSection_fixedSection__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/fixedSection/fixedSection */ "./components/fixedSection/fixedSection.js");
/* harmony import */ var _components_skills_skills__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/skills/skills */ "./components/skills/skills.js");
/* harmony import */ var _components_footer_footer__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/footer/footer */ "./components/footer/footer.js");
/* harmony import */ var _fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @fortawesome/fontawesome-svg-core */ "./node_modules/@fortawesome/fontawesome-svg-core/index.es.js");
/* harmony import */ var _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons */ "./node_modules/@fortawesome/free-brands-svg-icons/index.es.js");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "./node_modules/@fortawesome/free-solid-svg-icons/index.es.js");
/* harmony import */ var _components_aboutMe_aboutMe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../components/aboutMe/aboutMe */ "./components/aboutMe/aboutMe.js");
/* harmony import */ var _components_contacts_contacts__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../components/contacts/contacts */ "./components/contacts/contacts.js");

var _jsxFileName = "d:\\cv-khachik\\cv-khachik\\pages\\index.js";















_fortawesome_fontawesome_svg_core__WEBPACK_IMPORTED_MODULE_11__["library"].add(_fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_12__["fab"], _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_13__["fas"]);

function Index() {
  // Refs
  var containerRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var fixedSectionRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
  var headerRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null); // States

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState2 = Object(_babel_runtime_corejs2_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useState, 2),
      scrollAnim = _useState2[0],
      setScrollAnim = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])([]),
      _useState4 = Object(_babel_runtime_corejs2_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useState3, 2),
      frameLinks = _useState4[0],
      setFrameLink = _useState4[1];

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState6 = Object(_babel_runtime_corejs2_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useState5, 2),
      frameHeight = _useState6[0],
      setFrameHeight = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState8 = Object(_babel_runtime_corejs2_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useState7, 2),
      frameWidth = _useState8[0],
      setFrameWidth = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0),
      _useState10 = Object(_babel_runtime_corejs2_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useState9, 2),
      frameMargin = _useState10[0],
      setFrameMargin = _useState10[1];

  var setFrameLinks = function setFrameLinks() {
    // Set frame for header links
    var links = document.querySelectorAll('.nav-item'); // Get All links

    var setLinksArray = [];
    var link_margin = 0;
    links.forEach(function (link, index) {
      var clientWidth = window.innerWidth > 992 ? link.clientWidth : link.clientHeight; // Check window size

      setLinksArray[index] = {
        linksName: link.children[0].attributes.href.value.slice(1),
        linkWidth: clientWidth
      };
      var linkName = link.children[0].attributes.href.value.slice(1);
      var section = document.querySelector(".".concat(linkName));

      if (containerRef.current.offsetHeight - headerRef.current.clientHeight <= window.innerHeight + window.scrollY) {
        // When the scroll reaches the last section, changes the frame sizes
        setFrameHeight(link.clientHeight);
        setFrameWidth(link.clientWidth);
        setFrameMargin(link_margin);
      } else if (section.offsetTop - headerRef.current.offsetHeight - 1 <= window.pageYOffset && section.offsetTop + section.clientHeight > window.pageYOffset) {
        // When the scroll reaches the present section, changes the frame sizes
        setFrameHeight(link.clientHeight);
        setFrameWidth(link.clientWidth);
        setFrameMargin(link_margin);
      }

      link_margin += clientWidth;
    }); // The setFrameLink is for getting all links' names and width or height depended on the window with array

    setFrameLink(setLinksArray);
  };

  var handleScroll = function handleScroll() {
    fixedHeader();

    if (scrollAnim === true) {
      setFrameLinks();
    }
  };

  var fixedHeader = function fixedHeader() {
    // When pageYOffset is greater header's from height, header is fixes
    var header = headerRef.current;

    if (window.pageYOffset > header.clientHeight) {
      header.classList.add('fixed-header');
    } else {
      header.classList.remove('fixed-header');
    }
  };

  var handleClickLinks = function handleClickLinks(item, index) {
    setScrollAnim(false);
    var link = document.getElementsByClassName('nav-item')[index];
    var link_margin = 0;
    setFrameWidth(link.clientWidth);
    setFrameHeight(link.clientHeight);
    frameLinks.forEach(function (link, link_index) {
      if (link_index != index && link_index < index) {
        link_margin += link.linkWidth;
      }
    });
    setFrameMargin(link_margin); // When you click on links the scroll goes to that section

    var href = item.href.slice(1);

    if (href == 'home') {
      window.scrollTo({
        top: 0
      });
    } else {
      var offsetTop = document.querySelector(".".concat(href)).offsetTop;
      var top = offsetTop - headerRef.current.clientHeight;
      window.scrollTo({
        top: top
      });
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (scrollAnim === false) {
      // This function works when update the useEffect dependency
      window.addEventListener('scroll', handleScroll);
      setScrollAnim(true);
      return function () {
        // This return works before update the useEffect dependency
        window.removeEventListener('scroll', handleScroll);
      };
    }
  }, [scrollAnim]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    // Component did mount
    setFrameLinks();
    window.addEventListener('scroll', handleScroll);
    return function () {
      // component will unmount
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_1___default.a.Fragment, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 144
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_pageHead__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 145
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_fixedSection_fixedSection__WEBPACK_IMPORTED_MODULE_8__["default"], {
    fixedSectionRef: fixedSectionRef,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 146
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "w-100 position-absolute fixed-top",
    ref: containerRef,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 147
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_header_header__WEBPACK_IMPORTED_MODULE_6__["default"], {
    setFrameLinks: setFrameLinks,
    handleClickLinks: handleClickLinks,
    headerRef: headerRef,
    frameHeight: frameHeight,
    frameWidth: frameWidth,
    frameMargin: frameMargin,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 148
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_home_home__WEBPACK_IMPORTED_MODULE_7__["default"], {
    pictureHeight: fixedSectionRef,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 156
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_skills_skills__WEBPACK_IMPORTED_MODULE_9__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 157
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_aboutMe_aboutMe__WEBPACK_IMPORTED_MODULE_14__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 158
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_contacts_contacts__WEBPACK_IMPORTED_MODULE_15__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 159
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_footer_footer__WEBPACK_IMPORTED_MODULE_10__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160
    },
    __self: this
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ })

})
//# sourceMappingURL=index.js.6be01cc0d56f0650923c.hot-update.js.map