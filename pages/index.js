import React, { useState, useEffect, useRef } from 'react';
import PageHead from './pageHead';
import '../style.scss';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../components/header/header';
import Home from '../components/home/home';
import FixedSection from '../components/fixedSection/fixedSection';
import Skills from '../components/skills/skills';
import Footer from '../components/footer/footer';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import AboutMe from '../components/aboutMe/aboutMe';
import Contacts from '../components/contacts/contacts';

library.add(fab, fas);

function Index() {
	// Refs
	const containerRef = useRef(null);

	const fixedSectionRef = useRef(null);

	const headerRef = useRef(null);

	// States
	const [ scrollAnim, setScrollAnim ] = useState(true);

	const [ frameLinks, setFrameLink ] = useState([]);

	const [ frameHeight, setFrameHeight ] = useState(0);

	const [ frameWidth, setFrameWidth ] = useState(0);

	const [ frameMargin, setFrameMargin ] = useState(0);

	const setFrameLinks = () => {
		// Set frame for header links
		const links = document.querySelectorAll('.nav-item'); // Get All links
		let setLinksArray = [];
		var link_margin = 0;
		links.forEach((link, index) => {
			let clientWidth = window.innerWidth > 992 ? link.clientWidth : link.clientHeight; // Check window size
			setLinksArray[index] = {
				linksName: link.children[0].attributes.href.value.slice(1),
				linkWidth: clientWidth
			};
			let linkName = link.children[0].attributes.href.value.slice(1);
			let section = document.querySelector(`.${linkName}`);
			if (
				containerRef.current.offsetHeight - headerRef.current.clientHeight <=
				window.innerHeight + window.scrollY
			) {
				// When the scroll reaches the last section, changes the frame sizes
				setFrameHeight(link.clientHeight);
				setFrameWidth(link.clientWidth);
				setFrameMargin(link_margin);
			} else if (
				section.offsetTop - headerRef.current.offsetHeight - 1 <= window.pageYOffset &&
				section.offsetTop + section.clientHeight > window.pageYOffset
			) {
				// When the scroll reaches the present section, changes the frame sizes
				setFrameHeight(link.clientHeight);
				setFrameWidth(link.clientWidth);
				setFrameMargin(link_margin);
			}
			link_margin += clientWidth;
		});
		// The setFrameLink is for getting all links' names and width or height depended on the window with array
		setFrameLink(setLinksArray);
	};

	const handleScroll = () => {
		fixedHeader();
		if (scrollAnim === true) {
			setFrameLinks();
		}
	};

	const fixedHeader = () => {
		// When pageYOffset is greater header's from height, header is fixes
		const header = headerRef.current;
		if (window.pageYOffset > header.clientHeight) {
			header.classList.add('fixed-header');
		} else {
			header.classList.remove('fixed-header');
		}
	};

	const handleClickLinks = (item, index) => {
		setScrollAnim(false);
		let link = document.getElementsByClassName('nav-item')[index];
		var link_margin = 0;
		setFrameWidth(link.clientWidth);
		setFrameHeight(link.clientHeight);
		frameLinks.forEach((link, link_index) => {
			if (link_index != index && link_index < index) {
				link_margin += link.linkWidth;
			}
		});
		setFrameMargin(link_margin);
		// When you click on links the scroll goes to that section
		let href = item.href.slice(1);
		if (href == 'home') {
			window.scrollTo({
				top: 0
			});
		} else {
			let offsetTop = document.querySelector(`.${href}`).offsetTop;
			let top = offsetTop - headerRef.current.clientHeight;
			window.scrollTo({
				top: top
			});
		}
	};

	useEffect(
		() => {
			if (scrollAnim === false) {
				// This function works when update the useEffect dependency
				window.addEventListener('scroll', handleScroll);
				setScrollAnim(true);
				return () => {
					// This return works before update the useEffect dependency
					window.removeEventListener('scroll', handleScroll);
				};
			}
		},
		[ scrollAnim ]
	);

	useEffect(() => {
		// Component did mount
		setFrameLinks();
		window.addEventListener('scroll', handleScroll);
		return () => {
			// component will unmount
			window.removeEventListener('scroll', handleScroll);
		};
	}, []);

	return (
		<React.Fragment>
			<PageHead />
			<FixedSection fixedSectionRef={fixedSectionRef} />
			<div className="w-100 position-absolute fixed-top" ref={containerRef}>
				<Header
					setFrameLinks={setFrameLinks}
					handleClickLinks={handleClickLinks}
					headerRef={headerRef}
					frameHeight={frameHeight}
					frameWidth={frameWidth}
					frameMargin={frameMargin}
				/>
				<Home pictureHeight={fixedSectionRef} />
				<Skills />
				<AboutMe />
				<Contacts />
				<Footer />
			</div>
		</React.Fragment>
	);
}

export default Index;
