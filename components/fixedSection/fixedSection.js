import React from 'react';

function FixedSection({ fixedSectionRef }) {
	return (
		<section className="fixedSection">
			<div className="fixedSection-image">
				<img src="static/pictures/home.jpg" ref={fixedSectionRef} />
			</div>
		</section>
	);
}

export default FixedSection;
