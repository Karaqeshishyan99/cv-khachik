import React, { useEffect, useState, useRef } from 'react';
import Link from 'next/link';

const navbar = [
	{ href: '#home', name: 'Home' },
	{ href: '#skills', name: 'Skills' },
	{ href: '#aboutMe', name: 'About Me' },
	{ href: '#contacts', name: 'Contacts' }
];

function Header({ setFrameLinks, handleClickLinks, headerRef, frameHeight, frameWidth, frameMargin }) {
	const buttonRef = useRef(null);

	const [ frameSize, setFrameSize ] = useState({});

	const handleClick = (link, index) => {
		handleClickLinks(link, index);
	};

	const windowClick = (e) => {
		if (buttonRef.current.attributes[5].value == 'true') {
			// Check when menu is open then we are moving forward
			let path = e.path || e.composedPath(); // Get path of event

			if (!path.includes(headerRef.current) && window.innerWidth < 992) {
				// Check when didn't clicked at header and window display is small and if it is true new closes the menu of header
				buttonRef.current.click();
			}
		}
	};

	useEffect(
		() => {
			if (window.innerWidth < 992) {
				// Large devices
				setFrameSize({
					width: frameWidth + 'px',
					height: frameHeight + 'px',
					marginTop: frameMargin + 'px'
				});
			} else {
				// Small devices
				setFrameSize({
					width: frameWidth + 'px',
					marginLeft: frameMargin + 'px'
				});
			}
		},
		[ frameWidth, frameMargin ]
	);

	useEffect(() => {
		window.addEventListener('click', windowClick);
		return () => {
			window.removeEventListener('click', windowClick);
		};
	}, []);

	return (
		<header ref={headerRef}>
			<nav className="navbar navbar-expand-lg navbar-light">
				<Link href="/">
					<h3 className="navbar-brand logo m-0">
						{/* <img src="static/logo.png" title="logo" alt="logo" /> */}
						CV-Khachik
					</h3>
				</Link>
				<button
					ref={buttonRef}
					onClick={() => setFrameLinks()}
					className="navbar-toggler"
					type="button"
					data-toggle="collapse"
					data-target="#navbarNav"
					aria-controls="navbarNav"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span className="navbar-toggler-icon" />
				</button>
				<div className="collapse navbar-collapse" id="navbarNav">
					<ul className="navbar-nav ml-auto position-relative">
						<div className="frame-link" style={frameSize} />
						{navbar.map((item, index) => (
							<li className="nav-item" key={index}>
								<Link href={item.href}>
									<a className="nav-link" onClick={() => handleClick(item, index)}>
										{item.name}
									</a>
								</Link>
							</li>
						))}
					</ul>
				</div>
			</nav>
		</header>
	);
}

export default Header;
