import React from 'react';
import Link from 'next/link';
import NoSSR from 'react-no-ssr';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const social = [
	{ title: 'bitbucket', link: 'https://bitbucket.org/%7Beec8a889-19e6-44d0-9901-2d83ff29286d%7D/' },
	{ title: 'linkedin-in', link: 'https://www.linkedin.com/in/xachik-karaqeshishyan-27999a177/' },
	{ title: 'instagram', link: 'https://www.instagram.com/xcho_karaqeshishyan/' }
];

function Footer() {
	return (
		<footer className="footer">
			<div className="container">
				<div className="row">
					<div className="footer-copyRight col-12 col-sm-6">
						<div>2019 &copy; CV-Khachik</div>
						<div className="siteCreated">
							This site was created by me with React
							<span className="siteCreated-lovely">
								<NoSSR>
									<FontAwesomeIcon icon="heart" />
								</NoSSR>
							</span>
						</div>
					</div>
					<div className="footer-other col-12 col-sm-6">
						<div className="row justify-content-around">
							{social.map((item, index) => (
								// console.log(item);
								<div key={index} className="item">
									<Link href={item.link}>
										<a>
											<NoSSR>
												<FontAwesomeIcon icon={[ 'fab', item.title ]} />
											</NoSSR>
										</a>
									</Link>
									<div />
									<div />
									<div />
									<div />
									<div />
								</div>
							))}
						</div>
					</div>
				</div>
			</div>
		</footer>
	);
}

export default Footer;
