import React from 'react';
import Slider from 'react-slick';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NoSSR from 'react-no-ssr';

const slider_items = [
	{
		img: 'annakaraloans.JPG',
		companyName: 'Anna Kara Loans',
		webSite: 'https://www.annakaraloans.com/mortgage-calc-new',
		description: "I have been making mortgage calculator with javascript for annakaraloans's website.",
		location: 'Glendale, United State',
		date: 'Within 2 months'
	},
	{
		img: 'trainingDevelopment.jpg',
		description:
			'I have been doing online training with Frontend development and teached HTML, CSS, Bootstrap, JavaScript, jQuery skills in two months.',
		location: 'Glendale, United State',
		date: 'Within 2 months'
	},
	{
		img: 'projectnulla.svg',
		companyName: 'Project Nulla',
		webSite: 'https://projectnulla.com',
		description: 'In the Project nulla I worked as frontend developer. There I have been doing React and Redux.',
		location: 'Yerevan, Armenia',
		date: '2018 October - 2019 March'
	},
	{
		img: 'vectus.JPG',
		companyName: 'Vectus llc',
		webSite: 'https://vectus.am',
		description:
			'In the Vectus I worked as full stack web developer. There I have been doing the Drupal framework of php.',
		location: 'Yerevan, Armenia',
		date: '2019 March - July'
	}
];

function Experience() {
	const settings = {
		dots: false,
		className: 'center',
		infinite: true,
		centerPadding: '60px',
		slidesToShow: 3,
		swipeToSlide: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 1
				}
			}
		]
	};
	return (
		<section className="section experience-slider py-3">
			<div className="container">
				<NoSSR>
					<Slider {...settings}>
						{slider_items.map((item, index) => (
							<div key={index} className="experience-slider-item">
								{item.img ? (
									<div className="experience-slider-item-image mb-2">
										<img src={`static/company_logos/${item.img}`} />
									</div>
								) : null}
								<div className="experience-slider-item-info px-3">
									{item.location ? (
										<div className="experience-slider-item-info-location">
											<NoSSR>
												<FontAwesomeIcon icon="map-marker-alt" />
											</NoSSR>
											<span>{item.location}</span>
										</div>
									) : null}
									{item.date ? (
										<div className="experience-slider-item-info-date mb-4">
											<NoSSR>
												<FontAwesomeIcon icon="calendar-alt" />
											</NoSSR>
											<span>{item.date}</span>
										</div>
									) : null}
									{item.companyName ? (
										<div className="experience-slider-item-info-company mb-1">
											<h5 className="experience-slider-item-info-company-title mb-0">
												Company Name:&nbsp;
											</h5>
											<span className="experience-slider-item-info-company-name">
												{item.companyName}
											</span>
										</div>
									) : null}
									{item.webSite ? (
										<div className="experience-slider-item-info-company mb-1">
											<h5 className="experience-slider-item-info-company-title mb-0">
												Web Site:{' '}
											</h5>
											<a
												className="experience-slider-item-info-company-link"
												href={item.webSite}
												target="_blank"
											>
												{item.webSite}
											</a>
										</div>
									) : null}
									{item.description ? (
										<div className="mb-1">
											<h5 className="experience-slider-item-info-company-title mb-0 d-inline">
												Description:&nbsp;
											</h5>
											<p className="d-inline experience-slider-item-info-company-value">
												{item.description}
											</p>
										</div>
									) : null}
								</div>
							</div>
						))}
					</Slider>
				</NoSSR>
			</div>
		</section>
	);
}

export default React.memo(Experience);
