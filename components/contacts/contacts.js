import React from 'react';
import NoSSR from 'react-no-ssr';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const contactUsObjKeys = [
	{ iconPrefix: 'fas', icon: 'envelope', name: 'Gmail' },
	{ iconPrefix: 'fab', icon: 'skype', name: 'Skype' },
	{ iconPrefix: 'fas', icon: 'mobile', name: 'Phone' },
	{ iconPrefix: 'fas', icon: 'map-marker-alt', name: 'Address' }
];

const contactUsObjValues = [
	'karaqeshishyan.99@gmail.com',
	'Khachik Karakeshishyan',
	'+37491651165',
	'Yerevan, Armenia'
];

function Contacts() {
	return (
		<section className="section contacts">
			<div className="container">
				<h2 className="my-3 text-center text-white">Contacts</h2>
				<p className="contacts-description">
					if you have questions or suggestions you can contact me with the mentioned contacts.
				</p>
				<div className="row justify-content-center">
					<div className="col-12 col-md-8 col-lg-6 my-3">
						<form className="px-5">
							<div className="contacts-wrapper-input">
								<input type="text" name="name" placeholder="Name" />
							</div>
							<div className="contacts-wrapper-input">
								<input type="email" name="email" placeholder="E-mail" />
							</div>
							<div className="contacts-wrapper-input">
								<textarea type="text" name="message" placeholder="Message" rows="4" />
							</div>
							<div className="contacts-wrapper-input pt-2 text-center">
								<button className="button" type="submit" name="submit">
									Send messeage
								</button>
							</div>
						</form>
					</div>
					<div className="contacts-me col-12 col-md-8 col-lg-6 my-3">
						<div className="row justify-content-center">
							<div className="contacts-me-bracket-left col-2 px-0">
								<img src="static/icons/bracket-left.png" title="bracket" alt="icon" />
							</div>
							<div className="contacts-me-obj-keys col-7 col-sm-8 px-0 py-4 d-flex justify-content-around">
								<div className="contacts-me-obj-keys d-flex flex-column justify-content-between">
									{contactUsObjKeys.map((key, index) => (
										<span key={index}>
											<NoSSR>
												{key.iconPrefix == 'fab' ? (
													<FontAwesomeIcon icon={[ 'fab', key.icon ]} />
												) : (
													<FontAwesomeIcon icon={key.icon} />
												)}
											</NoSSR>
											&nbsp;{key.name}&nbsp;
											<span className="contacts-me-obj-keys-dots">:</span>
										</span>
									))}
								</div>
								<div className="contacts-me-obj-values d-flex flex-column justify-content-between">
									{contactUsObjValues.map((value, index) => <span key={index}>{value}</span>)}
								</div>
							</div>
							<div className="contacts-me-bracket-right col-2 px-0">
								<img src="static/icons/bracket-right.png" title="bracket" alt="icon" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}

export default Contacts;
