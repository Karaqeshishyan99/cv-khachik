import React from 'react';
import Experience from '../experience/experience';

function Home({ pictureHeight }) {
	return (
		<section className="home">
			<div
				className="for-home-info w-100 d-flex justify-content-center"
				style={{
					height: pictureHeight.current
						? pictureHeight.current.offsetHeight ? pictureHeight.current.offsetHeight : '100vh'
						: '100vh'
				}}
			>
				<div className="home-info">
					<div>
						<h4 className="home-info-title">Welcome to my CV site</h4>
						<p className="home-info-description">
							I'm a web developer. This is my CV site where you can get acquainted with my works and my
							professional skills and also you can contact me․
						</p>
					</div>
				</div>
			</div>
			<Experience />
		</section>
	);
}

export default Home;
