import React, { useState, useEffect } from 'react';
const { WOW } = typeof window !== 'undefined' && require('wowjs');

const makeing_layout = [
	{ img: 'html5.png', title: 'HTML5', alt: 'skill logos', animPos: 'bounceInLeft', animDelay: '0.5s' },
	{ img: 'css3.png', title: 'CSS3', alt: 'skill logos', animPos: 'bounceInLeft', animDelay: '0.2s' },
	{ img: 'sass.png', title: 'SASS', alt: 'skill logos', animPos: 'bounceInRight', animDelay: '0.2s' },
	{ img: 'bootstrap.png', title: 'Bootstrap4', alt: 'skill logos', animPos: 'bounceInRight', animDelay: '0.5s' }
];

const js_frontend = [
	{ img: 'js.png', title: 'JavaScript', alt: 'skill logos', animPos: 'bounceInLeft', animDelay: '0.3s' },
	{ img: 'jquery.png', title: 'Jquery', alt: 'skill logos', animPos: 'bounceInRight', animDelay: '0.3s' }
];

const js_frameworks = [
	{ img: 'reactjs.png', title: 'ReactJS', alt: 'skill logos', animPos: 'bounceInLeft', animDelay: '0.3s' },
	{ img: 'redux.png', title: 'Reudx', alt: 'skill logos', animPos: 'bounceInUp', animDelay: '0.3s' },
	{ img: 'nextjs.png', title: 'NextJS', alt: 'skill logos', animPos: 'bounceInRight', animDelay: '0.3s' }
];

const php = [
	{ img: 'php.png', title: 'PHP', alt: 'skill logos', animPos: 'bounceInLeft', animDelay: '0.3s' },
	{ img: 'sql.png', title: 'SQL', alt: 'skill logos', animPos: 'bounceInRight', animDelay: '0.3s' }
];

const programing_skills = [
	{ img: 'mvc.png', title: 'MVC', alt: 'skill logos', animPos: 'bounceInLeft', animDelay: '0.3s' },
	{ img: 'oop.png', title: 'OOP', alt: 'skill logos', animPos: 'bounceInUp', animDelay: '0.3s' },
	{ img: 'seo.jpg', title: 'SEO', alt: 'skill logos', animPos: 'bounceInRight', animDelay: '0.3s' }
];

function Skills(props) {
	useEffect(() => {
		// Wow initialization
		var wow = new WOW({
			live: false
		});
		wow.init();
	}, []);

	return (
		<section className="section skills">
			<div className="container">
				<div className="my-3 text-center">
					<h2 className="skills-title">Skills</h2>
					<p className="skills-description">My professional skills in web development</p>
				</div>
				<div className="skills-part makeing-layout">
					{makeing_layout.map((item, index) => (
						<div
							key={index}
							className={`skills-part-item wow ${item.animPos}`}
							data-wow-duration="2s"
							data-wow-delay={item.animDelay}
						>
							<img src={`static/skills_logos/${item.img}`} title={item.title} alt={item.alt} />
						</div>
					))}
				</div>
				<div className="skills-part js-frontend">
					{js_frontend.map((item, index) => (
						<div
							key={index}
							className={`skills-part-item wow ${item.animPos}`}
							data-wow-duration="2s"
							data-wow-delay={item.animDelay}
						>
							<img src={`static/skills_logos/${item.img}`} title={item.title} alt={item.alt} />
						</div>
					))}
				</div>
				<div className="skills-part js-frameworks">
					{js_frameworks.map((item, index) => (
						<div
							key={index}
							className={`skills-part-item wow ${item.animPos}`}
							data-wow-duration="2s"
							data-wow-delay={item.animDelay}
						>
							<img src={`static/skills_logos/${item.img}`} title={item.title} alt={item.alt} />
						</div>
					))}
				</div>
				<div className="skills-part php">
					{php.map((item, index) => (
						<div
							key={index}
							className={`skills-part-item wow ${item.animPos}`}
							data-wow-duration="2s"
							data-wow-delay={item.animDelay}
						>
							<img src={`static/skills_logos/${item.img}`} title={item.title} alt={item.alt} />
						</div>
					))}
				</div>
				<div className="skills-part programing-skills">
					{programing_skills.map((item, index) => (
						<div
							key={index}
							className={`skills-part-item wow ${item.animPos}`}
							data-wow-duration="2s"
							data-wow-delay={item.animDelay}
						>
							<img src={`static/skills_logos/${item.img}`} title={item.title} alt={item.alt} />
						</div>
					))}
				</div>
			</div>
		</section>
	);
}

export default Skills;
