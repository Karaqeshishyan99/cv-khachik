import React from 'react';

function AboutMe() {
	return (
		<section className="section aboutMe">
			<div className="container">
				<h2 className="my-3 text-center">About Me</h2>
				<div className="d-flex flex-wrap justify-content-center">
					<div className="col-12">
						<div className="aboutMe-block my-3 mx-auto">
							<div className="aboutMe-block-picture">
								<img src="static/pictures/aboutMe.jpg" />
							</div>
						</div>
					</div>
					<div className="aboutMe-description col-10">
						I'm Khachik Karaqeshishyan, 20 years old live in Armenia, Yerevan. I graduated the high school
						in 2017. Along with that I have finished the course in the field of web development in
						<a href="https://codecenter.am/" target="_blank">
							&nbsp;Codecenter Armenia&nbsp;
						</a>and already a two year I am occupying with that. I received my first order from Anna Kara
						Loans company and completed it, after that I have been teaching an online course to someone and
						after all I worked in some companies as a web developer and gained experiences from them
					</div>
				</div>
			</div>
		</section>
	);
}

export default AboutMe;
