const withPlugins = require('next-compose-plugins');
const withCSS = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');
const withStylus = require('@zeit/next-stylus');

const nextConfig = withCSS(
	withSass({
		// cssModules: true,
		webpack: (config) => {
			// config.module.rules.push = {
			// 	test: /\.schema\.js|canner\.def\.js$/,
			// 	// include by files and folders who match the pattern.
			// 	include: [ path.join(__dirname, 'schema'), path.join(__dirname, 'node_modules') ],
			// 	use: [
			// 		{ loader: 'canner-schema-loader' },
			// 		{
			// 			loader: 'babel-loader',
			// 			options: {
			// 				presets: [
			// 					// overwrite some next/babel default settings.
			// 					[
			// 						'next/babel',
			// 						{
			// 							'preset-env': { modules: 'commonjs' },
			// 							'transform-runtime': {
			// 								helpers: true,
			// 								polyfill: true,
			// 								regenerator: true
			// 							}
			// 						}
			// 					]
			// 				]
			// 			}
			// 		}
			// 	]
			// };
			return config;
		}
	})
);

module.exports = withPlugins([], nextConfig);

// CSS
// cssModules: true,
// compiled css classes and ides
// CSS
